# DOT Buoy Data Collection

This repository contains configuration files, [Systemd](https://freedesktop.org/wiki/Software/systemd/) service files, and utility scripts to manage the data collection process on the DOT Buoy.

## Installation

Download the latest Debian package from the *Downloads* page of this repository. Install the package on the DOT system then run the `update-apps` command:

``` shellsession
$ dpkg -i dot-config_VERSION_any.deb
$ update-apps
```
