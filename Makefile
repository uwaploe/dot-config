DATE    ?= $(shell date +%FT%T%z)
VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || \
			cat $(CURDIR)/.version 2> /dev/null || echo v0)

SYSDIR = $(ROOTDIR)/lib/systemd/system-shutdown
BINDIR = $(ROOTDIR)/usr/local/bin
CFGDIR = $(ROOTDIR)/usr/local/etc
SVCDIR = $(ROOTDIR)/etc/systemd/system
PPPDIR = $(ROOTDIR)/etc/ppp
SYSCFGDIR = $(ROOTDIR)/etc
LOGDIR = $(ROOTDIR)/var/log/journal
BOOTDIR = $(ROOTDIR)/boot

.PHONY: install

install: $(CFGDIR) $(SVCDIR) $(SYSDIR) $(BINDIR) $(PPPDIR) $(LOGDIR) $(BOOTDIR)
	cp -av alarmclock.sh $(SYSDIR) && cp -av rc.local $(SYSCFGDIR)
	cp -v journald.conf $(SYSCFGDIR)/systemd/
	cd bin && cp -av * $(BINDIR)
	cd config && cp -av * $(CFGDIR)
	cd systemd && cp -av * $(SVCDIR)
	cd ppp && cp -av * $(PPPDIR) && chmod 750 $(PPPDIR)/chatscripts
	cd boot && cp -av * $(BOOTDIR)/

$(BINDIR) $(CFGDIR) $(SVCDIR) $(SYSDIR) $(PPPDIR) $(SYSCFGDIR) $(LOGDIR) $(BOOTDIR)::
	@mkdir -p $@
