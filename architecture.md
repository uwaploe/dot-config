# DOT Buoy Data Sampling

This document describes the high-level architecture of the DOT data sampling and storage software. This repository contains the system configuration files and utility software. The the source code for the data acquisition software is contained in separate repositories.

## Overview

The data acquisition programs are run as Systemd services and publish their collected data records to a central Message Broker ([Redis](http://redis.io/). An archiving service subscribes to this published data and collects the records into files. New files are created on a periodic basis, compressed and stored in an "outbox" directory. Once an hour, a PPP link is established to a server at APL using an Iridium modem and the contents of the outbox are uploaded to the server.

## Data Acquisition

| **Data set**      | **Program** | **Service**     | **Repository**                                      |
|-------------------|-------------|-----------------|-----------------------------------------------------|
| GNSS              | gnssdaq     | gnssdaq.service | <https://bitbucket.org/uwaploe/gnssdaq/src/master/> |
| Met Station       | rdcr300     | rdcr300.service | <https://bitbucket.org/uwaploe/rdcr300/src/master/> |
| Attitude/Pressure | rdsens      | rdsens.service  | <https://bitbucket.org/uwaploe/dotsens/src/master/> |


### GNSS

The GNSS pseudo-range data is collected every 30 seconds. The data records are stored in the Septentrio Binary Format. The setup commands and the `gnssdaq` configuration file are located in the `config/gnss` subdirectory of this respository. The service is configured to start whenever the GNSS receiver is connected to the USB bus.

### Met Station

The Met Station is running a custom program which wakes the device every 15 minutes and outputs 1Hz data records in NMEA format for 80 seconds. The `rdcr300` program reads the records and publishes them to the Data Broker and calculates a 60 second average which is published to a separate channel.

### Attitude/Pressure

The attitude and pressure sensors are sampled together and stored in [Protocol Buffer](https://developers.google.com/protocol-buffers) format. The format is described in a separate repository, <https://bitbucket.org/uwaploe/dotpb/src/master/>.

## Data Storage

The data is archived by two separate instances of the [rdarchiver](https://bitbucket.org/uwaploe/rdarchiver/src/master/) service. One instance is for the GNSS/Attitude/Pressure data and the other is for the engineering (Met Station) data. The configuration files for both instances are located in the `config/rdarchiver` subdirectory.

## Building

[Nfpm](https://nfpm.goreleaser.com) is required to build the Debian package. Run the `builddist.sh` script in the top level directory to create a new package file.
