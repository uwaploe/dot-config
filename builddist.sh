#!/usr/bin/env bash

: ${KVERS=4.1.15}

set -e
vers=$(git describe --tags --always --dirty --match=v* 2> /dev/null || echo v0)

VERSION="${vers#v}"

rm -rf dist
mkdir dist

export VERSION KVERS
nfpm pkg --packager deb --config nfpm-${KVERS}.yaml --target dist/

[[ "$1" = "release" ]] && {
    cd dist
    ../bbupload.sh -E ../.env dot-config-${KVERS}_${VERSION}_any.deb
}
